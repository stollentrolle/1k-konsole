#!/bin/bash
#
# extrahiere den aktuellen git-commit-hash
#
git log -1 | head -1 | cut -b 8-18 > gitversion.tex && pdflatex --shell-escape $1

