# Retrospiele auf 64 LEDs

Jugend hackt OER 

## Git-Repository

Dieses OER lebt in einem öffentlichen Git-Repository im Ordner `OER`:

[1k-Konsole](https://gitlab.com/stollentrolle/1k-konsole)

## Erstellen eines PDF 

```sh
bash gitversion.sh oer-hauptdatei.tex
```
Dieser 'Umweg' extrahiert die aktuelle Git-Revision, so dass diese in
das Dokument eingebaut wird.

Ohne Zugang zur git-Version bekommt man das PDF mit den folgenden Befehlen:
```sh
echo $(date +%Y%m%d%H%M) > gitversion.tex
pdflatex --shell-escape oer-hauptdatei.tex
```
Hierbei wird die Git-Revision durch die aktuelle Uhrzeit ersetzt.

## Erstellen des Ablaufplans

Der [Ablaufplan](../2023-Pacman/pacman.gv) ist von Hand erstellt.
Einziger Helfer war `grep def pacman.py`. Aus dem Plan im
GraphViz-Format erstellt man eine Grafik mit dem Kommando
```sh
dot -Tpng pacman.gv > ablaufplan.png
```

## Umwandlung in Word/Office/RTF

**ACHTUNG:** Dieser Weg ist in allen erprobten Varianten extrem
empfindlich und versionsabhängig.

### Methode 1

```sh
latex2rtf oer-hauptdatei.tex
```

Der Konverter ist schon älter und kennt manches Moderne nicht.
Aber die grundsätzliche Konversion läuft durch.

Das größte Problem sind die Umlaute, die in UTF-8 bei der Konversion
im Zieldokument einfach fehlen. Abhilfe schafft es, alle
Umlaute in einer Kopie durch ihr ASCII-LaTeX-Äquivalent zu ersetzen.
Dieses muss genau dem entsprechen, was `latex2rtf` versteht.

Auf meinem  Raspberry Pi 400 unter Arch Linux läuft das folgende
problemlos:
```bash
rm -rf _rtf
mkdir -p _rtf
cp *tex *.png _rtf/
cd _rtf
for datei in *.tex; do
sed -i -e s:'ä':'"a{}':g -e s:'ö':'"o{}':g -e s:'ü':'"u{}':g -e s:'Ä':'"A{}':g -e s:'Ö':'"O{}':g -e s:'Ü':'"U{}':g -e s:'ß':'\\ss{}':g $datei
done
latex2rtf oer-hauptdatei.tex
```

Mit dieser Konversion bekommt man ein RTF-Dokument, dass sich
in LibreOffice Writer Version 7.0.4.2 unter Debian Linux 11.5
problemlos öffnen lässt. Zwei größere Punkte bleiben noch:

+ die `\python{}`-Befehle werden nicht erkannt
+ das Titelbild wird nicht übernommen

Kleinere Punkte sind Tabellen mit Rahmen oder Trennzeichen im Wort
'Teil-neh-mer*-in-nen'.

Ich halte die Konversion für durchaus gelungen und ausreichend, so
dass auch Menschen ohne Umgang mit LaTeX sich mit geringem Aufwand
und dem PDF als Vorlage dafür, wie es aussehen soll, eine eigener
Version dieses OER erstellen können, mit ihren eigenen Anpassungen
und Anmerkungen.

### Methode 2

Es gibt ein neueres Tool, um LaTeX in HTML und auch in ODT
umzuwandeln. Dieses kommt direkt mit den Umlauten zurecht.
```sh
make4ht --shell-escape --format odt oer-hauptdatei.tex
```

Dieses Tool mag allerdings die `tikzpicture`-Umgebungen nicht, so dass
man diese auskommentieren oder entfernen muss.

Auch die Header und Footer lassen sich nicht umwandeln und wollen
gerne auskommentiert sein.
```tex
%% \usepackage{scrlayer-scrpage}
%% \lohead{}
%% \cohead{}
%% \rohead{}
%% \lofoot{\textcolor{gray}{Version \input{gitversion.tex}}}
%% \cofoot{\textcolor{gray}{\copyright{} \the\year{} Roland Härter}}
%% \rofoot{\thepage{}/\pageref{LastPage}}
```

Außerdem muss man für alle eingebundenen Bilder eine `xbb`-Datei mit
dem Kommando
```sh
for i in *png ; do extractbb -v -x $i ; done
```
erstellen.

Der komplette Workflow für die Konversion:
```bash
git pull
for i in *png ; do extractbb -v -x $i ; done
dot -Tpng -Gsize=9,15\! -Gdpi=300 -oablaufplan.png ../2023-Pacman/pacman.gv
bash gitversion.sh oer-hauptdatei.tex
bash gitversion.sh oer-hauptdatei.tex
make4ht --shell-escape --format odt oer-hauptdatei.tex
make4ht --shell-escape --format odt oer-hauptdatei.tex
lowriter oer-hauptdatei.odt
```

Sicherheitshalber sollte man alle Artefakte der Konversion regelmäßig
entfernen. Dies geht mit den folgenden Kommandos:
```bash
for i in aux bbl bcf blg idx ilg ind log out nav snm toc run.xml ttt; do rm -f *${i}; done
for i in 4ct 4tc 4og dvi idv lg odt out.ps xref; do rm -f *${i}; done
rm -f oer-hauptdatei*tmp
rm -f oer-hauptdatei*svg
```

#### Versionsprobleme bei Methode 2

Grundsätzlich gilt, dass die Umwandlung in andere Formate ein
schwieriges Geschäft ist. Außerdem gilt, dass die hier aufgeführten
Konversionen _stets bedeuten_, dass eine gelungene Umwandlung das
ursprüngliche Dokument einigermaßen wiedergibt. Man kann es lesen
und auf einen ersten, flüchtigen Blick scheint alles bei der Konversion
irgendwie erhalten geblieben zu sein.

+ Debian Bullseye 11.7 hat eine Version, mit der die Konversion in ein
  ODT funktioniert.

    - Word 2016 kann die Datei erst nach einem Reparaturversuch öffnen
      und zeigt dann alle Bilder und alle Sternchen in einer
      Einheitsgöße.

    - Wordpad Windows10 22H2 kann die Datei öffnen und sie sieht
      einigermaßen ok aus. Jedoch sind alle Sternchen nur eine kleine
      Lücke im Wort.

    - LibreOffice Write 7.0.4.2 erzielt das beste Ergebnis. Eine von
      hier als docx exportierte Version lässt sich dann auch
      problemlos in Word 2016 öffnen.

+ Arch Linux hat eine Version, mit der die Konversion nicht
  gelingt. Das habe ich nicht weiter verfolgt. Vielleicht fehlt nur
  eine Package.


## ToDo

1. Ein PDF mit Erläuterungen des Quelltextes erstellen
2. Test von alternativer Hardware (Mikrocontroller und LED-Matrixen)

