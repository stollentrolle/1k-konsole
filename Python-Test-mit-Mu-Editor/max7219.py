#
# github.com/adafruit/Adafruit_CircuitPython_MAX7219/blob/master7examples/max7219_simpletest.py
#
import time
from board import TX, RX, A1
import busio
import digitalio
from adafruit_max7219 import matrices

mosi = TX
clk = RX
cs = digitalio.DigitalInOut(A1)

spi = busio.SPI(clk, MOSI=mosi)

matrix = matrices.Matrix8x8(spi, cs)

while True:
    print("cycle start")
    # all lit up
    matrix.fill(True)
    matrix.show()
    time.sleep(1)

    # all off
    matrix.fill(False)
    matrix.show()
    time.sleep(1)
    """
    # one column of leds lit
    for i in range(8):
        matrix.pixel(1,i,1)
    matrix.show()
    time.sleep(1)
    for j in range(4):
        matrix.scroll(1,0)
        matrix.show()
        time.sleep(1)
    """
    botschaft = "Hallo"
    for zeichen in botschaft:
        matrix.fill(0)
        matrix.text(zeichen,0,0,2)
        matrix.show()
        time.sleep(1)

    # scroll the last character off the display
    for i in range(8):
        matrix.scroll(-1,0)
        matrix.show()
        time.sleep(0.5)

    # scroll a string across the display
    for pixel_position in range( len(botschaft) * 8):
        matrix.fill(0)
        matrix.text(botschaft, -pixel_position, 0)
        matrix.show()
        time.sleep(0.25)