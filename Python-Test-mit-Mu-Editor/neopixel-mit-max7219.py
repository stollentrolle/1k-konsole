import time
from board import TX, RX, A1, NEOPIXEL
import busio
import digitalio
from adafruit_max7219 import matrices
import neopixel

mosi = TX
clk = RX
cs = digitalio.DigitalInOut(A1)
spi = busio.SPI(clk, MOSI=mosi)
matrix = matrices.Matrix8x8(spi, cs)

led = neopixel.NeoPixel(NEOPIXEL, 1)

def wheel(pos):
    if pos < 0 or pos > 255:
        return 0, 0, 0
    if pos < 85:
        return int(255 - pos * 3), int(pos * 3), 0
    if pos < 170:
        pos -= 85
        return 0, int(255 - pos * 3), int(pos * 3)
    pos -= 170
    return int(pos * 3), 0, int(255 - (pos * 3))

led.brightness = 0.3

i = 0
while True:
    i = (i+1) % 256
    botschaft = str(i)
    # scroll a string across the display
    for pixel_position in range( len(botschaft) * 8):
        matrix.fill(0)
        matrix.text(botschaft, -pixel_position, 0)
        matrix.show()
        time.sleep(0.1)
    # setze den NeoPixel auf Farbe wheel(i)
    led.fill(wheel(i))
    time.sleep(0.1)