import time
import board
import neopixel
led = neopixel.NeoPixel(board.NEOPIXEL, 1)

def wheel(pos):
    if pos < 0 or pos > 255:
        return 0, 0, 0
    if pos < 85:
        return int(255 - pos * 3), int(pos * 3), 0
    if pos < 170:
        pos -= 85
        return 0, int(255 - pos * 3), int(pos * 3)
    pos -= 170
    return int(pos * 3), 0, int(255 - (pos * 3))

led.brightness = 0.3

i = 0
while True:
    i = (i+1) % 256
    led.fill(wheel(i))
    time.sleep(0.1)