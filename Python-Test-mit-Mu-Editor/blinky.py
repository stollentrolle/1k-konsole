# Schreibe hier Deinen Code :-)
import board
import digitalio
import time

#print(" *** Willkommen! *** ")

led = digitalio.DigitalInOut(board.D13)
led.direction = digitalio.Direction.OUTPUT

i = 0.0
while True:
    print(((i),))
    i = i + 10.0
    led.value = True
    time.sleep(0.5)
    led.value = False
    time.sleep(0.5)
    if i > 999.99:
        i = 0.0