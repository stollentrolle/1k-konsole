import getch

print ("Willkommen zum Zeicheneingabetest.")
while True:
    zeichen = getch.getch()
    try:
        zeichen = zeichen.decode('UTF-8')
    except:
        zeichen = "X"
    print (f"Zeichen {zeichen} eingelesen")
    if zeichen == 'q':
        break
