import board
import neopixel
import time

led = neopixel.NeoPixel(board.A1, 256, brightness=1.0)
led.brightness = 0.10

def main():
    i = -1
    while True:
        i = (i + 1) % 256  # run from 0 to 255
        led[i] = (i,abs(128-i),255-i)
        time.sleep(0.1)
        if i == 255:
            time.sleep(10.1)
            led.fill((0,0,0))

if __name__ == '__main__':
    main()
