#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
'''
Grundgerüst für einen Pacman-Klon auf einer NeoPixel-Matrix unter BSD 2-clause-Lizenz
Copyright 2023 Roland Härter, r.haerter@wut.de

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
THE POSSIBILITY OF SUCH DAMAGE.
'''
import board
import time
import neopixel
import random
import sys

max_xpos = 8
max_ypos = 8
anzahl = max_xpos * max_ypos
pixel_pin = board.A1
ORDER = neopixel.GRB
ZICKZACK = False

def umrechnen(xpos, ypos):
    if ZICKZACK:
        return umrechnen_zickzack(xpos, ypos)
    else:
        return umrechnen_normal(xpos, ypos)

def umrechnen_normal( xpos, ypos ):
    return ypos * max_ypos + xpos

def umrechnen_zickzack(xpos, ypos):
    if ypos%2 == 0:
        pixelnummer = ypos * max_xpos + xpos
    else:
        pixelnummer = (ypos+1) * max_xpos - xpos - 1
    return pixelnummer

def rueckweg_pixel_zu_koordinaten_liste_erstellen():
    liste = []
    for _ in range(anzahl):
        liste.append(_)
    for x in range(max_xpos):
        for y in range(max_ypos):
            liste[umrechnen(x,y)] = (x,y)
    return liste

def alles_anzeigen(spielfeld, pixels):
    for i in range(len(spielfeld)):
        pixels[i] = spielfeld[i]
    pixels.show()

# ---------------------------------------------------------
# Hier beginnt das eigentliche Spiel
SPIELER = (255, 0, 0)
BODEN = (50, 50, 50)
WAND = (0, 255, 0)
GEIST1 = (255, 255, 0)
GEIST2 = (0, 255, 255)
GEIST3 = (255, 0, 255)
GEISTER = [ GEIST1, GEIST2, GEIST3 ]
# Abkürzungen für das Schreiben der Level-Matrix
S = SPIELER
G = GEIST1
H = GEIST2
I = GEIST3
W = WAND
B = BODEN
FINDELISTE = rueckweg_pixel_zu_koordinaten_liste_erstellen()

def initialisieren(spielfeld):
    if len(spielfeld) == 0:
        for i in range(anzahl):
            spielfeld.append(BODEN)
    else:
        for i in range(len(spielfeld)):
            spielfeld[i] = BODEN
    x, y = zufallskoordinate()
    spielfeld[umrechnen(x,y)] = SPIELER
    waende_erzeugen(spielfeld)
    return (x, y)

def geister_erzeugen(spielfeld):
    for i in range(len(GEISTER)):
        x,y = zufallskoordinate()
        while (spielfeld[umrechnen(x,y)] != BODEN):
            x,y = zufallskoordinate()
        spielfeld[umrechnen(x,y)] = GEISTER[i]

def geister_bewegen(spielfeld):
    result = 0
    for i in range(len(GEISTER)):
        x,y = geist_finden(spielfeld, GEISTER[i])
        result = result + geist_bewegen(spielfeld,i+1,x,y)
    return result

def geist_bewegen(spielfeld,geist,x,y):
    if geist == 1:
        x_neu = (x + random.randrange(-1,2,1)) % max_xpos
        y_neu = (y + random.randrange(-1,2,1)) % max_ypos
        if spielfeld[umrechnen(x_neu,y_neu)] == WAND:
            pass
        elif spielfeld[umrechnen(x_neu,y_neu)] == SPIELER:
            return 1
        elif spielfeld[umrechnen(x_neu,y_neu)] in GEISTER:
            pass
        else:
            spielfeld[umrechnen(x,y)] = BODEN
            spielfeld[umrechnen(x_neu,y_neu)] = GEISTER[geist-1]
    elif geist == 2:
        x_neu = (x + random.randrange(-1,2,1)) % max_xpos
        y_neu = (y + random.randrange(-1,2,1)) % max_ypos
        if spielfeld[umrechnen(x_neu,y_neu)] == WAND:
            pass
        elif spielfeld[umrechnen(x_neu,y_neu)] == SPIELER:
            return 2
        elif spielfeld[umrechnen(x_neu,y_neu)] in GEISTER:
            pass
        else:
            spielfeld[umrechnen(x,y)] = BODEN
            spielfeld[umrechnen(x_neu,y_neu)] = GEISTER[geist-1]
    elif geist == 3:
        x_neu = (x + random.randrange(-1,2,1)) % max_xpos
        y_neu = (y + random.randrange(-1,2,1)) % max_ypos
        if spielfeld[umrechnen(x_neu,y_neu)] == WAND:
            pass
        elif spielfeld[umrechnen(x_neu,y_neu)] == SPIELER:
            return 4
        elif spielfeld[umrechnen(x_neu,y_neu)] in GEISTER:
            pass
        else:
            spielfeld[umrechnen(x,y)] = BODEN
            spielfeld[umrechnen(x_neu,y_neu)] = GEISTER[geist-1]
    else:
        print (f"Der Geist {geist} ist nicht implementiert.")
    return 0

def geist_finden(spielfeld,geist):
    for i in range(len(spielfeld)):
        if spielfeld[i] == geist:
            return FINDELISTE[i]

def geist_ermitteln( spielfeld, x, y ):
    for g in GEISTER:
        if ((x,y) == geist_finden(spielfeld, g)):
            resultat = g
            break
    for i in (i for i,x in enumerate(GEISTER) if x == resultat):
        return i+1

def zufallskoordinate():
    x = random.randrange(0,max_xpos,1)
    y = random.randrange(0,max_ypos,1)
    return (x,y)

def waende_erzeugen(spielfeld, anzahl=3):
    for i in range(anzahl):
        x1, y1 = zufallskoordinate()
        x2, y2 = zufallskoordinate()
        wand_erzeugen(spielfeld,x1,y1,x2,y2)

def wand_erzeugen(spielfeld,x1,y1,x2,y2):
    if x2 < x1:
        x1, x2 = x2, x1
    if y2 < y1:
        y1, y2 = y2, y1
    for i in list(range(x1,x2)):
        if spielfeld[umrechnen(i,y1)] == BODEN:
            spielfeld[umrechnen(i,y1)] = WAND
    for i in list(range(y1,y2)):
        if spielfeld[umrechnen(x2,i)] == BODEN:
            spielfeld[umrechnen(x2,i)] = WAND

def level_laden(spielfeld, pixels, level):
    if int(level) == 0:
        level_1(spielfeld)
    elif int(level) == 1:
        level_2(spielfeld)
    else:
        print (f"Der Level {level} exisitiert nicht.")
        return False
    alles_anzeigen(spielfeld, pixels)
    return True

def level_1(spielfeld):
    my_spielfeld = [
        B, B, B, B, B, B, B, B,
        B, W, B, W, W, W, W, B,
        G, W, B, B, B, W, B, H,
        B, W, W, W, B, W, B, W,
        B, B, B, W, B, W, B, B,
        B, W, B, B, S, B, W, B,
        B, W, W, W, W, W, W, B,
        B, B, B, B, I, B, B, B,
    ]
    for i in range(len(spielfeld)):
        spielfeld[i] = my_spielfeld[i]

def level_2(spielfeld):
    my_spielfeld = [
        B, B, B, B, B, B, B, B,
        B, B, B, B, B, B, B, B,
        B, B, B, B, B, B, B, B,
        B, B, B, B, B, B, B, B,
        B, B, B, B, B, B, B, B,
        B, B, B, B, B, B, B, B,
        B, G, B, B, H, B, B, I,
        B, B, B, B, B, B, B, B,
    ]
    for i in range(len(spielfeld)):
        spielfeld[i] = my_spielfeld[i]

def waende_erneuern(spielfeld):
    for i in range(len(spielfeld)):
        if spielfeld[i] == WAND:
            spielfeld[i] = BODEN
    waende_erzeugen(spielfeld)

def spieler_bewegen ( spielfeld, x, y, x_neu, y_neu ):
    ergebnis = 0
    if spielfeld[umrechnen(x_neu,y_neu)] == BODEN:
        spielfeld[umrechnen(x,y)] = BODEN
        spielfeld[umrechnen(x_neu,y_neu)] = SPIELER
        x = x_neu
        y = y_neu
    elif spielfeld[umrechnen(x_neu,y_neu)] == WAND:
        pass
    elif spielfeld[umrechnen(x_neu,y_neu)] in GEISTER:
        ergebnis = geist_ermitteln(spielfeld, x_neu, y_neu)
    return (x,y,ergebnis)

def male_schwarz(spielfeld):
    for i in range(len(spielfeld)):
        spielfeld[i] = (0,0,0)

def male_verloren(spielfeld):
    male_schwarz(spielfeld)
    for i in [ 0, 7, 9, 14, 18, 21, 27, 28, 35, 36, 42, 45, 49, 54, 56, 63 ]:
        spielfeld[i] = (50,0,0)

def male_gewonnen(spielfeld):
    male_schwarz(spielfeld)
    for i in [ 23, 30, 37, 40, 44, 49, 51, 58 ]:
        spielfeld[i] = (0,42,0)

def hauptprogramm():
    global GEISTER
    if len(GEISTER) < 3:
        GEISTER = [ GEIST1, GEIST2, GEIST3 ]
    pixels = neopixel.NeoPixel(pixel_pin, max_xpos*max_ypos, brightness=0.05,
                    auto_write=False, pixel_order=ORDER)
    leben = [ 12, 1, 1, 1 ]
    wartezeit_schlussbild = 5
    spielfeld = []
    x, y = initialisieren(spielfeld)
    x_neu, y_neu = x, y
    geister_erzeugen(spielfeld)
    alles_anzeigen(spielfeld, pixels)
    # Game-Loop
    while True:
        steuerzeichen = sys.stdin.read(1).lower()
        if steuerzeichen == 'q':
            wartezeit_schlussbild = 0
            break
        elif steuerzeichen == 'w':
            if y > 0:
                y_neu = y - 1
        elif steuerzeichen == 'a':
            if x > 0:
                x_neu -= 1
        elif steuerzeichen == 's':
            if y < ( max_ypos - 1 ):
                y_neu += 1
        elif steuerzeichen == 'd':
            if x < ( max_xpos - 1 ):
                x_neu += 1
        elif steuerzeichen == 'l':
            waende_erneuern(spielfeld)
        elif steuerzeichen == '1':
            level_laden(spielfeld, pixels, 0)
            x,y = geist_finden(spielfeld, SPIELER)
            x_neu, y_neu = x, y
        elif steuerzeichen == '2':
            level_laden(spielfeld, pixels, 1)
            x,y = geist_finden(spielfeld, SPIELER)
            x_neu, y_neu = x, y
        else:
            pass
        x, y, result = spieler_bewegen ( spielfeld, x, y, x_neu, y_neu )
        x_neu, y_neu = x, y
        if result > 0:
            leben[result] -= 1
        for i in range(len(GEISTER)):
            if leben[i+1] < 1:
                a,b = geist_finden(spielfeld,GEISTER[i])
                spielfeld[umrechnen(a,b)] = BODEN
                del GEISTER[i]
                del leben[i+1]
                break
        if len(GEISTER) < 1:
            GEISTER=[GEIST1,GEIST2,GEIST3,]
            leben=[23,9,9,9]
            level_laden(spielfeld, pixels, 1)
        result = geister_bewegen(spielfeld)
        if result > 0:
            leben[0] = leben[0] - 1
            if leben[0] == 0:
                male_verloren(spielfeld)
                alles_anzeigen(spielfeld, pixels)
                break
        alles_anzeigen(spielfeld, pixels)
    time.sleep(wartezeit_schlussbild)
    pixels.deinit()
# Hier endet das eigentliche Spiel
# ---------------------------------------------------------
def go():
    hauptprogramm()

if __name__ == '__main__':
    pass
