import board
import time
import neopixel
import random
import sys

max_xpos = 32
max_ypos = 8
anzahl = max_xpos * max_ypos
pixel_pin = board.A1
ORDER = neopixel.GRB
ZICKZACK = True

def umrechnen(xpos, ypos):
    if ZICKZACK:
        return umrechnen_zickzack(xpos, ypos)
    else
        return umrechnen_normal(xpos, ypos)

def umrechnen_zickzack(xpos, ypos):
    if ypos%2 == 0:
        print('even ',end='')
        pixelnummer = ypos * max_xpos + xpos
    else:
        print(' odd ',end='')
        pixelnummer = (ypos+1) * max_xpos - xpos - 1
    print(f"{xpos},{ypos}:{pixelnummer} ")
    return pixelnummer%256

def rueckweg_pixel_zu_koordinaten_liste_erstellen():
    liste = []
    for _ in range(anzahl):
        liste.append(_)
    for x in range(max_xpos):
        for y in range(max_ypos):
            liste[umrechnen(x,y)] = (x,y)
    return liste

led = neopixel.NeoPixel(board.A1, 256, brightness=0.05)
led.brightness = 0.05

led.fill((0,0,0))

rueckwegliste = rueckweg_pixel_zu_koordinaten_liste_erstellen()

res = umrechnen(10, 0)
print(f"{res} aus x:{rueckwegliste[res][0]} und y:{rueckwegliste[res][1]}")
led[res]=(234,230,0)


res = umrechnen(15, 2)
print(f"{res} aus x:{rueckwegliste[res][0]} und y:{rueckwegliste[res][1]}")
led[res]=(234,0,0)

res = umrechnen(27, 2)
print(f"{res} aus x:{rueckwegliste[res][0]} und y:{rueckwegliste[res][1]}")
led[res]=(0,234,0)

res = umrechnen(9, 4)
print(f"{res} aus x:{rueckwegliste[res][0]} und y:{rueckwegliste[res][1]}")
led[res]=(0,0,234)


res = umrechnen(0, 6)
print(f"{res} aus x:{rueckwegliste[res][0]} und y:{rueckwegliste[res][1]}")
led[res]=(230,230,234)
