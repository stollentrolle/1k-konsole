#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

'''
Winterkurs 2020 Junior Uni
Grundgerüst für ein Spiel auf einer NeoPixel-Matrix unter BSD 2-clause-Lizenz

Copyright 2020 Roland Härter, Wuppertal

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
THE POSSIBILITY OF SUCH DAMAGE.
'''

# Notwendige Module laden
import board
import time
import neopixel
import random
import sys
# Technische Daten
# Das Testfeld ist 8x8 Pixel 'groß'
max_xpos = 8
max_ypos = 8
anzahl = max_xpos * max_ypos
# An welchem Pin ist D_in der LED-Matrix angeschlossen
pixel_pin = board.A1
# The order of the pixel colors - RGB or GRB. Some NeoPixels have red and green reversed!
# For RGBW NeoPixels, simply change the ORDER to RGBW or GRBW.
ORDER = neopixel.GRB

# trans() berechnet aus der Position in der Matrix die Position der LED
def trans( xpos, ypos ):
    return ypos * max_ypos + xpos

# Hier wird spielfeld[] in pixels[] umkopiert und angezeigt
def alles_anzeigen(spielfeld, pixels):
    for i in range(len(spielfeld)):
        pixels[i] = spielfeld[i]
    pixels.show()

# ---------------------------------------------------------
# Hier beginnt das eigentliche Spiel

# Die Dinge, die es gibt, werden hier über ihre Farbe definiert.
# Farben werden als Tupel von (Rot, Grün, Blau) angegeben.
SPIELER = (255, 0, 0)
BODEN = (50, 50, 50)
WAND = (0, 255, 0)
GEIST1 = (255, 255, 0)
GEIST2 = (0, 255, 255)
GEIST3 = (255, 0, 255)
GEISTER = [ GEIST1, GEIST2, GEIST3 ]

# Einmal am Start das Spielfeld einrichten
def initialisieren(spielfeld):
    if len(spielfeld) == 0:
        for i in range(anzahl):
            spielfeld.append(BODEN)
    else:
        for i in range(len(spielfeld)):
            spielfeld[i] = BODEN
    # zufälligen Startpunkt für den SPIELER erzeugen
    x, y = zufallskoordinate()
    spielfeld[trans(x,y)] = SPIELER
    waende_erzeugen(spielfeld)
    # Spielerkoordinate zurückgeben
    return (x, y)

# einmal am Start (jedes Levels) die Geister erzeugen
def geister_erzeugen(spielfeld):
    for i in range(len(GEISTER)):
        x,y = zufallskoordinate()
        while (spielfeld[trans(x,y)] != BODEN):
            x,y = zufallskoordinate()
        spielfeld[trans(x,y)] = GEISTER[i]

# Hier bewegen sich die Geister
# Alle Begegnungen mit dem Spieler werden als Wert zurück gegeben
def geister_bewegen(spielfeld):
    result = 0
    for i in range(len(GEISTER)):
        x,y = geist_finden(spielfeld, GEISTER[i])
        result = result + geist_bewegen(spielfeld,i+1,x,y)
    return result

# Hier die ausführliche Funktion mit eigener Funktion für jeden Geist
# Aus Bequemlichkeit werden die Geister als 1, 2 und 3 hier übergeben
# Die Geister geben ihren binären Stellenwert zurück, also Geist 1
# 001b = 1, Geist 2 010b = 2 und Geist 3 100b = 4
def geist_bewegen(spielfeld,geist,x,y):
    if geist == 1:
        # -1 oder 0 oder 1 würfeln und zur aktuellen Position addieren
        x_neu = (x + random.randrange(-1,2,1)) % max_xpos
        y_neu = (y + random.randrange(-1,2,1)) % max_ypos
        # Dann die neue Position prüfen
        if spielfeld[trans(x_neu,y_neu)] == WAND:
            # WAND --> nur eine Meldung ausgeben
            #print("Aua, die WAND getroffen. Ich dachte, Geister koennen da hindurch...")
            pass
        elif spielfeld[trans(x_neu,y_neu)] == SPIELER:
            # SPIELER --> eine Meldung aus- und True zurückgeben
            #print ("1: Ha, da ist der Spieler. Hab ich dich erwischt!")
            return 1
        elif spielfeld[trans(x_neu,y_neu)] in GEISTER:
            # Die anderen GEISTER --> nur eine Meldung ausgeben
            #print ("Hallo Kollege, fein dich zu treffen :-)")
            pass
        else:
            # Der bisher einzige Fall, in dem der GEIST1 sich bewegt,
            # indem die bisherige Position auf BODEN gesetzt wird
            spielfeld[trans(x,y)] = BODEN
            # und der GEIST die neue Position einnimmt
            spielfeld[trans(x_neu,y_neu)] = GEISTER[geist-1]
    elif geist == 2:
        # -1 oder 0 oder 1 würfeln und zur aktuellen Position addieren
        x_neu = (x + random.randrange(-1,2,1)) % max_xpos
        y_neu = (y + random.randrange(-1,2,1)) % max_ypos
        # Dann die neue Position prüfen
        if spielfeld[trans(x_neu,y_neu)] == WAND:
            # WAND --> nur eine Meldung ausgeben
            #print("Aua, die WAND getroffen. Ich dachte, Geister koennen da hindurch...")
            pass
        elif spielfeld[trans(x_neu,y_neu)] == SPIELER:
            # SPIELER --> eine Meldung aus- und True zurückgeben
            #print ("2: Ha, da ist der Spieler. Hab ich dich erwischt!")
            return 2
        elif spielfeld[trans(x_neu,y_neu)] in GEISTER:
            # Die anderen GEISTER --> nur eine Meldung ausgeben
            #print ("Hallo Kollege, fein dich zu treffen :-)")
            pass
        else:
            # Der bisher einzige Fall, in dem der GEIST2 sich bewegt,
            # indem die bisherige Position auf BODEN gesetzt wird
            spielfeld[trans(x,y)] = BODEN
            # und der GEIST die neue Position einnimmt
            spielfeld[trans(x_neu,y_neu)] = GEISTER[geist-1]
    elif geist == 3:
        # -1 oder 0 oder 1 würfeln und zur aktuellen Position addieren
        x_neu = (x + random.randrange(-1,2,1)) % max_xpos
        y_neu = (y + random.randrange(-1,2,1)) % max_ypos
        # Dann die neue Position prüfen
        if spielfeld[trans(x_neu,y_neu)] == WAND:
            # WAND --> nur eine Meldung ausgeben
            #print("Aua, die WAND getroffen. Ich dachte, Geister koennen da hindurch...")
            pass
        elif spielfeld[trans(x_neu,y_neu)] == SPIELER:
            # SPIELER --> eine Meldung aus- und True zurückgeben
            #print ("3: Ha, da ist der Spieler. Hab ich dich erwischt!")
            return 4
        elif spielfeld[trans(x_neu,y_neu)] in GEISTER:
            # Die anderen GEISTER --> nur eine Meldung ausgeben
            #print ("Hallo Kollege, fein dich zu treffen :-)")
            pass
        else:
            # Der bisher einzige Fall, in dem der GEIST3 sich bewegt,
            # indem die bisherige Position auf BODEN gesetzt wird
            spielfeld[trans(x,y)] = BODEN
            # und der GEIST die neue Position einnimmt
            spielfeld[trans(x_neu,y_neu)] = GEISTER[geist-1]
    else:
        print ("Der Geist {} ist nicht implementiert.".format(geist))
    return 0

# Die Geister sind ledglich eine Farbe in der Liste 'spielfeld[]'
def geist_finden(spielfeld,geist):
    for i in range(len(spielfeld)):
        if spielfeld[i] == geist:
            x = i%max_ypos
            y = i//max_ypos
            return x, y

# Wenn man wissen will, welchen Geist man gefunden hat, muss man rückwärts suchen
# Der Rückgabewert entspricht der Nummer des Geistes und damit auch dem Index in
# der Liste leben[] - so kann man direkt im Hauptprogramm ein Leben abziehen.
def geist_ermitteln( spielfeld, x, y ):
    for g in GEISTER:
        if ((x,y) == geist_finden(spielfeld, g)):
            res = g
            break
    # Da die Geister ihre Position in der Liste wechseln können,
    # muss der Rückgabewert korrekt ermittelt werden.
    for i in (i for i,x in enumerate(GEISTER) if x == res):
        # print ("Found GEIST{}".format(i+1))
        return i+1

# Eine zufällige Koordinate erwürfeln und zurück geben
def zufallskoordinate():
    # die beiden Koordinaten x und y würfeln
    x = random.randrange(0,max_xpos,1)
    y = random.randrange(0,max_ypos,1)
    return (x,y)

# 3 zufällige Wände erzeugen.
# Das gibt in den meisten Fällen _kein_ geeignetes Labyrinth.
def waende_erzeugen(spielfeld, anzahl=3):
    for i in range(anzahl):
        x1, y1 = zufallskoordinate()
        x2, y2 = zufallskoordinate()
        wand_erzeugen(spielfeld,x1,y1,x2,y2)

# Wände erzeugen.
# Diese Funktion ist noch ziemlich doof.
def wand_erzeugen(spielfeld,x1,y1,x2,y2):
    # Koordinaten sortieren
    if x2 < x1:
        x1, x2 = x2, x1
    if y2 < y1:
        y1, y2 = y2, y1
    # So eine Art 'L' zeichnen
    for i in list(range(x1,x2)):
        if spielfeld[trans(i,y1)] == BODEN:
            spielfeld[trans(i,y1)] = WAND
    for i in list(range(y1,y2)):
        if spielfeld[trans(x2,i)] == BODEN:
            spielfeld[trans(x2,i)] = WAND

# Feste Labyrinthe für jeden Level erzeugen
# Hier die Hauptfunktion
def level_laden(spielfeld, pixels, level):
    if int(level) == 0:
        level_1(spielfeld)
    elif int(level) == 1:
        level_2(spielfeld)
    elif int(level) == 2:
        level_3(spielfeld)
    else:
        print ("Dieser Level exisitiert nicht.")
        return False
    alles_anzeigen(spielfeld, pixels)
    return True

# Level 1
# Bei einem statisch generierten Level habe ich als Entwickler
# die volle Kontrolle und kann alle Elemente nach meinem Geschmack
# platzieren :-)
def level_1(spielfeld):
    print ("Willkommen auf Level 1")
    # Abkürzungen für das Schreiben der Matrix
    S = SPIELER
    G = GEIST1
    H = GEIST2
    I = GEIST3
    W = WAND
    B = BODEN
    # Fest vorgegebene Matrix als Startpunkt
    my_spielfeld = [
        E, E, E, E, E, E, E, E,
        E, E, E, E, E, E, E, E,
        S, W, B, B, B, W, B, H,
        B, W, W, W, B, W, B, W,
        B, B, B, D, B, W, B, B,
        H, W, B, B, S, B, W, B,
        H, W, B, B, S, B, W, B,
        H, W, B, B, S, B, W, B,
        B, W, W, W, W, W, W, B,
        B, B, I, B, I, B, B, B,
    ]
    # Um die Daten tatsächlich zu erhalten, muss man sie umkopieren
    # Stichwort 'call by value' versus 'call by reference' in C
    # --> https://www.quora.com/What-is-call-by-value-in-Python
    for i in range(len(spielfeld)):
        spielfeld[i] = my_spielfeld[i]

# Level 2
def level_2(spielfeld):
    print ("Willkommen auf Level 2")
    # Abkürzungen für das Schreiben der Matrix
    S = SPIELER
    G = GEIST1
    H = GEIST2
    I = GEIST3
    W = WAND
    B = BODEN
    # Fest vorgegebene Matrix ohne jede Wand als Startpunkt
    my_spielfeld = [
        B, B, B, B, B, B, B, B,
        B, B, B, S, B, B, B, B,
        B, B, B, B, B, B, B, B,
        B, B, B, B, B, B, B, B,
        B, B, B, E, B, B, B, B,
        B, B, C, B, B, B, B, B,
        B, G, B, B, H, B, B, I,
        W, B, B, B, B, B, B, B,
    ]
    for i in range(len(spielfeld)):
        spielfeld[i] = my_spielfeld[i]

# Level 3
def level_3(spielfeld):
    print ("Willkommen auf Level 3")
    # Abkürzungen für das Schreiben der Matrix
    S = SPIELER
    G = GEIST1
    H = GEIST2
    I = GEIST3
    W = WAND
    B = BODEN
    # Fest vorgegebene leere Matrix als Startpunkt und Kopiervorlage
    # HINWEIS: Eine leere Matrix ohne Spieler und Geister funktioniert _nicht_!
    my_spielfeld = [
        B, B, B, B, B, B, B, B,
        B, B, G, B, B, B, B, B,
        B, B, B, B, B, B, B, B,
        B, B, B, B, B, B, B, B,
        B, B, B, B, B, B, B, B,
        B, B, B, B, B, B, B, B,
        B, B, B, B, B, B, B, B,
        B, B, B, B, B, B, B, B,
    ]
    for i in range(len(spielfeld)):
        spielfeld[i] = my_spielfeld[i]

# Die generierten Wände können ziemlich ungeeignet sein.
# Mit dieser Funktion kann man sie neu erzeugen lassen.
def labyrinth_erneuern(spielfeld):
    # Alle vorhandenen Wände löschen
    for i in range(len(spielfeld)):
        if spielfeld[i] == WAND:
            spielfeld[i] = BODEN
    # Die vorhandene Funktion waende_erzeugen() aufrufen
    waende_erzeugen(spielfeld)

# Den SPIELER von x,y nach x_neu,y_neu bewegen
# Hier erfolgt eine Kollisionsprüfung
def move ( spielfeld, x, y, x_neu, y_neu ):
    # In ergebnis kann man eine Aktion des Spielers ins Hauptprogramm übermitteln
    ergebnis = 0
    # Prüfen, ob am neuen Ort BODEN ist, dann kann man dahin
    if spielfeld[trans(x_neu,y_neu)] == BODEN:
        # Zuerst die alte Position mit der Hintergrundfarbe übermalen
        # und dann den SPIELER an der neuen Stelle zeichnen.
        spielfeld[trans(x,y)] = BODEN
        spielfeld[trans(x_neu,y_neu)] = SPIELER
        # die neuen Koordinaten übernehmen
        x = x_neu
        y = y_neu
    # Wände werden ignoriert, und lediglich eine Meldung ausgegeben
    elif spielfeld[trans(x_neu,y_neu)] == WAND:
        #print ("*** Bumm. Da ist eine Wand.")
        pass
    elif spielfeld[trans(x_neu,y_neu)] in GEISTER:
        # Hier könnte man nun Prüfen, ob der Spieler ein PowerUp hat und nur dann
        # dem Geist ein Leben nimmt. Dazu muss es PowerUps geben, und die müssen Funktionen haben ...
        #print ("*** Ah, ein Geist.")
        ergebnis = geist_ermitteln(spielfeld, x_neu, y_neu)
    # Die gültigen neuen Spielerkoordinaten zurück geben
    return (x,y,ergebnis)

def male_schwarz(spielfeld):
    for i in range(len(spielfeld)):
        spielfeld[i] = (0,0,0)

def male_spielende(spielfeld):
    male_schwarz(spielfeld)
    for i in [ 0, 7, 9, 14, 18, 21, 27, 28, 35, 36, 42, 45, 49, 54, 56, 63 ]:
        spielfeld[i] = (50,0,0)

def male_gewonnen(spielfeld):
    male_schwarz(spielfeld)
    for i in [ 23, 30, 37, 40, 44, 49, 51, 58 ]:
        spielfeld[i] = (0,42,0)

def hauptprogramm():
    global GEISTER
    # Beim Wiederaufrufen, also wenn man nochmal spielen will, ist die
    # Liste GEISTER eventuell teilweise und wenn man gewonnen hat ganz leer.
    if len(GEISTER) < 3:
        GEISTER = [ GEIST1, GEIST2, GEIST3 ]
    # Die gesamten LEDs als ein Strang, ohne auto_write
    pixels = neopixel.NeoPixel(pixel_pin, max_xpos*max_ypos, brightness=0.05,
                    auto_write=False, pixel_order=ORDER)
    # Im Spiel hat man mehrere Leben. Die Geister haben auch Leben. Diese alle zusammen
    # stecken in einer Liste: leben[0] -> du, der Spieler, leben[1] -> GEIST1, ...
    leben = [ 69, 23, 23, 23 ]
    # Am Ende den Schlussbildschirm eine Weile anzeigen
    wartezeit_schlussbild = 5
    # Um zu wissen, was wo ist, braucht es eine eigene Datenstruktur
    spielfeld = []
    # alles einmal einrichten
    x, y = initialisieren(spielfeld)
    x_neu, y_neu = x, y
    # Die Geister ins Spiel bringen
    geister_erzeugen(spielfeld)
    # Nach allen Initialisierungen alles einmal anzeigen
    alles_anzeigen(spielfeld, pixels)
    # Endlosschleife, in der man unbegrenzt innerhalb
    #                 des Spielfelds herumlaufen kann
    while True:
        # genau ein Zeichen von der Tastatur lesen und in einen Kleinbuchstaben wandeln
        steuerzeichen = sys.stdin.read(1).lower()
        # q beendet die Endlosschleife
        if steuerzeichen == 'q':
            wartezeit_schlussbild = 0
            break
        # Laufen bis zum Rand mit w a s d
        elif steuerzeichen == 'w':
            if y > 0:
                y_neu = y - 1
        elif steuerzeichen == 'a':
            if x > 0:
                x_neu -= 1
        elif steuerzeichen == 's':
            if y < ( max_ypos - 1 ):
                y_neu += 1
        elif steuerzeichen == 'd':
            if x < ( max_xpos - 1 ):
                x_neu += 1
        # Labyrinth erneuern mit 'L'
        elif steuerzeichen == 'l':
            labyrinth_erneuern(spielfeld)
        # Zum Testen der festen Level diese direkt laden mit Ziffern 1, 2, 3
        # Dabei muss man die neue Position des Spielers auch übernehmen
        elif steuerzeichen == '1':
            level_laden(spielfeld, pixels, 0)
            x,y = geist_finden(spielfeld, SPIELER)
            x_neu, y_neu = x, y
        elif steuerzeichen == '2':
            level_laden(spielfeld, pixels, 1)
            x,y = geist_finden(spielfeld, SPIELER)
            x_neu, y_neu = x, y
        elif steuerzeichen == '3':
            level_laden(spielfeld, pixels, 2)
            x,y = geist_finden(spielfeld, SPIELER)
            x_neu, y_neu = x, y
        # alles andere ignorieren und nichts tun - das bewegt trotzdem die Geister!
        else:
            pass
        # Hier jetzt den SPIELER bewegen
        # Rückgabewerte sind die neue Position x,y und ein Ergebnis result
        # In result wird zurück gegeben, wenn der Spieler einem Geist ein Leben genommen hat
        x, y, result = move ( spielfeld, x, y, x_neu, y_neu )
        # x_neu,y_neu zurücksetzten, auch wenn der Spieler sich nicht bewegt hat
        x_neu, y_neu = x, y
        # result auswerten
        if result > 0:
            #print (" --> getroffen, result = {}, leben = {}".format(result,leben))
            leben[result] -= 1
        # Hier jetzt prüfen, ob noch Geister leben und die ohne Leben entfernen
        for i in range(len(GEISTER)):
            if leben[i+1] < 1:
                print ("Du hast den Geist GEIST{} beseitigt.".format(i+1))
                # Den Geist durch BODEN ersetzen, so dass er nicht mehr zu sehen ist
                a,b = geist_finden(spielfeld,GEISTER[i])
                spielfeld[trans(a,b)] = BODEN
                del GEISTER[i]   # den Geist auch aus der Liste entfernen
                del leben[i+1]   # und seinen Punktestand von 0 auch entfernen
                break            # wenn ich die Liste kürze, muss ich danach aussteigen
        # Wenn es keine GEISTER mehr gibt, hat der Spieler gewonnen
        if len(GEISTER) < 1:
            print ("\n   *** Du hast gewonnen. *** \n")
            male_gewonnen(spielfeld)
            alles_anzeigen(spielfeld,pixels)
            break
        # jetzt ziehen die Geister
        result = geister_bewegen(spielfeld)
        if result > 0:      # Subtiles Detail: if result == True reagiert nur auf '1'
            leben[0] = leben[0] - 1
            welche_geister = [ "", "Geist 1", "Geist 2", "Geist 1 und Geist 2", "Geist 3", "Geist 1 und Geist 3", "Geist 2 und Geist 3", "Alle drei Geister" ]
            welches_verb   = [ "", "hat",     "hat",     "haben",               "hat",     "haben",               "haben",               "haben" ]
            print ("{} {} dich erwischt. ".format(welche_geister[result],welches_verb[result]),end="")
            if leben[0] == 0:
                print("Du hast verloren.\n\n\tGAME OVER!\n")
                male_spielende(spielfeld)
                alles_anzeigen(spielfeld, pixels)
                break
            else:
                # Die deutsche Sprache hat ihren Preis
                # Bei einem Rest-Leben ein 's' an 'verbliebene' anhaengen
                if leben[0] == 1:
                    s = "s"
                else:
                    s = ""
                print ("Du hast noch {} verbliebene{} Leben.".format(leben[0], s))
        # Alles anzeigen, also auch das, was sich geändert hat.
        alles_anzeigen(spielfeld, pixels)
    # Nach dem Spielende 5 Sekunden warten und dann alle Ressourcen freigeben
    time.sleep(wartezeit_schlussbild)
    pixels.deinit()

# Hier endet das eigentliche Spiel
# ---------------------------------------------------------

# Abkürzung, um beim Laden Buchstaben zu sparen
def go():
    hauptprogramm()

# Kein automatischer Start, damit man die Konsole zum Steuern nutzen kann
if __name__ == '__main__':
    pass
