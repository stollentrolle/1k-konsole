import board
import neopixel
import time
import random

# Ein einfaches Ratespiel
def ratespiel():
    weiter = True
    print ("Zahlenraten")
    print ("-----------\n")
    print ("Ich denke mir eine Zahl von 1 bis 100")
    print ("Du versuchst sie zu raten. Abbrechen kannst du mit 'q'")
    zahl = random.randint(1,100)
    while weiter == True:
        ratewort = input("Welche Zahl ist es: ")
        if ratewort.lower() == 'q':
            weiter = False
            break
        ratewert = int(ratewort)
        if ratewert == zahl:
            print ("Hurra, du hast es gefunden.")
            weiter = False
            break
        else:
            if ratewert < zahl:
                print ("Zu klein")
            else:
                print ("Zu gross")

# Eine Abkürzung, um beim Tippen zu sparen
def go():
    ratespiel()