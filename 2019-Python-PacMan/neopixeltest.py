import board
import neopixel
import time

# Nur den onboard Neopixel nutzen
#led = neopixel.NeoPixel(board.NEOPIXEL, 1)
# Die LED-Matrix 8x8 == 64 nutzen mit voller Helligkeit
# ACHTUNG: Passende Energieversorgung erforderlich!
# Am USB-Anschluss ist der maximale Strom begrenzt.
# In normalen Räumen reicht brightness=0.05 oder 0.1
led = neopixel.NeoPixel(board.A1, 64, brightness=1.0)

def wheel(pos):
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 0 or pos > 255:
        return 0, 0, 0
    if pos < 85:
        return int(255 - pos * 3), int(pos * 3), 0
    if pos < 170:
        pos -= 85
        return 0, int(255 - pos * 3), int(pos * 3)
    pos -= 170
    return int(pos * 3), 0, int(255 - (pos * 3))

led.brightness = 0.3

def main():
    i = 0
    while True:
        i = (i + 1) % 256  # run from 0 to 255
        led.fill(wheel(i))
        time.sleep(0.1)

if __name__ == '__main__':
    main()
