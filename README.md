# 1k-Konsole

Die 1k-Konsole hat in der ursprünglichen Planung 32x32 = 1024 RGB-LEDs.

Sie basiert konzeptionell auf [LEDmePlay](http://mithotronic.de/ledmeplay.php)
von Thomas Laubach, Krefeld und Michael Roßkopf, Ratingen.

Aufgrund von _Schwierigkeiten_ wurde sie dann zu einer 8x8-LED-Matrix
aus WS2812-LEDs reduziert, die sich mit CircuitPython programmieren lässt.

## Fazit

Dieses Projekt ist definitiv _kein_ Projekt für Programmieranfänger.

Der Start mit einem begrenzten aber schon spielbaren Grundgerüst ist
weiterhin ein guter Ansatz. Aber die Komplexität des Grundgerüsts
erfordert ein gewisses Maß an Vorkenntnissen bei den Teilnehmern.
Und dieses 'gewisse Maß' ist eher im Bereich Fortgeschrittene
anzusiedeln.

Es stellt sich die Frage, ob dieses Projekt aus W&T-Sicht als gescheitert erklärt
werden muss. Die von uns erreichten Jugendlichen sind in der Regel Anfänger
mit bestenfalls geringen Grundkenntnissen.

Die mangelhafte Stabilität unter Linux ist wie bei der technisch neueren Retrokonsole.
Wenn man erst den Editor Mu-Code startet und danach das Laufwerk `CIRCUITPY`
einhängt, wird reproduzierbar die Datei mit dem Spiel beschädigt, wenn diese
schon in Mu Code geöffnet ist bzw. geöffnet wird, weil sie noch in der Liste
der zuletzt benutzten Dateien steht. Nach einem
Dateisystemcheck ist die Datei dann entfernt und nur mit Methoden der
Datenrettung wiederherstellbar. Das ist ein unschöner Zustand.
Es ist auch eine unnötige zusätzliche Schwierigkeit, die möglicherweise mit der
Konfiguration der verwendeten Debian-Linux-Installation zusammenhängt.
Unter Windows funktioniert es deutlich besser als unter Linux mit `thunar`
als Dateimanager. Unter Windows ist standardmäßig der Automount im Explorer
aktiviert, so dass dieses Problem nicht auftritt.

Die Problematik mit dem komplexen Programm als Startpunkt bleibt bestehen.
Abhilfe könnte schaffen, den Programmcode in Ablaufrichtung zu erklären,
also im Text unten mit `spiel.go()` zu starten. Aber die grundsätzlichen
Schwierigkeiten mit Nur-Spielern und mit der Überforderung von Anfängern
bleiben bestehen, siehe auch die Erläuterungen bei der Retrokonsole.

## Die Hardware und der Editor

Die verwendete Hardware der Konsole besteht aus einem Adafruit Metro M4 und
einer 8x8-LED-Matrix, die einem LED-Streifen entspricht. In der verwendeten
Matrix laufen alle Zeilen von rechts nach links und nicht im Zickzack.

Die Basisplatine Metro M4 wird über USB mit einem PC oder Laptop verbunden.
Über diese Leitung zeigt sich die Platine als USB-Laufwerk und als 
USB-Seriell-Adapter. Ich sehe also im Explorer ein Laufwerk mit Namen
`CIRCUITPY` und im Gerätemanger eine neue serielle Schnittstelle.

Um sich mit den Niederungen der Technik nicht auseinandersetzen zu
müssen, hat [Nicholas H.Tollervey](https://ntoll.org/) den Editor
[Mu Code](https://codewith.mu/) entwickelt. Dieser Editor ermöglicht
es, den Code auf dem CircuitPython-Gerät zu bearbeiten und parallel
dazu seriell auf die dort laufende Python-Konsole zuzugreifen.
Dazu muss man beim Start des Editors die Konsole eingesteckt haben
und als Modus `CircuitPython` wählen.

Die LED-Matrix wird vom Metro M4 mit Strom und Daten versorgt.
Sie braucht 5 Volt, GND und als Datenpin den Pin A1. Jede
WS2812-LED enthält ein Schieberegister, in dem sie aus dem Datenstrom
24 Bit nimmt und diese als Rot, Grün und Blau mit jeweils 8 Bit
interpretiert. Auch diese Niederungen der Technik werden einem
abgenommen, da man mit Hilfe einer Bilbliothek jede einzelne
WS2812-LED direkt ansprechen kann und ihr ein RGB-Tupel übergibt.

## Konzept

In den ersten Iterationen dieses Angebots hat sich herausgestellt, dass
ein Start mit einem bereits spielbaren Startprogramm richtig Spaß macht
und sehr motivierend wirkt. Diese Motivation ist stark genug, um sich
mit dem mit gut 500 Zeilen kommentiertem Python recht langen und auch
schon ziemlich komplexen Programm auseinanderzusetzen. Diese
Auseinandersetzung braucht je nach Vorkenntnis der TN mehr
oder weniger viel Unterstützung. Es ist hilfreich, wenn der Kursleiter
und die eventuell vorhandenen Mentoren bzw. Assistenten sich im Vorfeld
schon mit Python und mit dem Programm beschäftigt haben. Für die
Assistenten bzw. Mentoren ist dies nicht zwingend erforderlich.

## Anwendung in der Junior Uni 2019

In der Junior Uni fand es als Kurs für 15 11- bis 14-jährige statt.
Hier wurde am Beamer der Code gezeigt und erläutert. Es wurden
auch Modifikationen am Code vorgeführt, um diese zu erklären, wenn
von den TN entsprechende Fragen kamen. In diesem Vor-Corona-Setting
haben sich je zwei TN einen Laptop und eine Konsole geteilt und
in einer Art Pair-Programming gearbeitet.

Die Begeisterung zeigte sich darin, dass die TN am Ende der einzelnen
Kurstermine gar nicht gehen wollten und erst beim finalen Gong der
Junior Uni gegangen sind.

## Anwendung im Jugend hackt Lab Wülfrath 2022

In entspannter Atmosphäre mit zwei Teilnehmern, circa 12 und 14 Jahre
alt, ließ sich ganz einfach loslegen. Jeder hatte einen Laptop mit
Mu Code als Editor, eine eigene Konsole und ich habe zum Start eine
kleine Einführung gegeben. Erstaunlicherweise kannte sogar der circa
12jährige TN das Spiel Pacman. In der Einführung brauchte ich also
das Spiel nicht zu erklären. So habe ich mich auf die Idee der Matrix
und die grundlegenden Definitionen beschränkt, z.B. wer hat welche
Farbe und wo findet man das im Code.

Im weiteren kamen dann Fragen auf, wie z.B. Wo sind die Farben definiert?
Wie kann ich die ändern? Kann man als Spieler die Geister besiegen bzw.
diesen Schaden zufügen? Hier konnte ich in diesem kleinen
Rahmen einfach auf Zeilennummern im Code verweisen und wir haben
ohne Beamer direkt jeder in seinem Code gesehen, was vorliegt und
ich habe vereinzelte Dinge erklärt.

Ein TN hat sich als Schluss- oder Start-Bildschirm überlegt, einen
Pacman zu zeigen. Gelber Umriss auf schwarzem Grund.

