/*  PACMAN Regeln:
 *
 *  Das kleine, gelbe, gefräßige Ding in der Mitte des Spiels ist der Pacman.
 *  Diese Figur kann man mit Hilfe der Tastatur mit WASD bewegen
 *
 *  Ziel ist es, alle weißen Punkte in dem Feld einzusammeln (zu fressen).
 *  Sind alle Punkte weg, kommt man in den nächsten Level.
 *
 *  Vorsicht vor den Geistern, eine Berührung damit kostet ein Leben.
 *  Offizielle Geister-Farben: Rot, Pink, Hellblau, Orange.
 *  Frisst Pacman aber einen blinkenden Punkt, kann er für
 *  kurze Zeit die Geister fressen (das gibt extra Punkte)
 *  und ist nicht angreifbar. Die Geister werden dann blau.
 *  Bekommen die Geister ihre normale Farbe zurück, sind sie wieder gefährlich.
 *
 *  In der Mitte des Spielfeldes tauchen ab und zu Obststücke auf.
 *  Für diese Obststücke gibt es Sonderpunkte, wenn PacMan sie frisst.
 *
 *  https://www.kikisweb.de/spiele/regeln/pacman.htm
 *
 *  Viel ausführlicher wird das Spiel hier beschrieben:
 *  http://www.gamasutra.com/view/feature/132330/the_pacman_dossier.php
 */

/* Die LED-Matrix in Betrieb nehmen */
#include <RGBmatrixPanel.h>
//#include <level.h>
/* R1 G1 B1 R2 G2 B2 are on 2 3 4 5 6 7 */
#define OE   9
#define LAT 10
#define A   A0
#define B   A1
#define C   A2
#define D   A3
#define CLK A4 // USE THIS ON METRO M4

/*  Damit man sehen kann, dass alle Punkte gefressen wurden,
 *  muss man wissen, wie viele am Anfang vorhanden sind.
 */

uint16_t anzahl_punkte = 0;
// normale Version 32x32
RGBmatrixPanel matrix(A, B, C, D, CLK, LAT, OE, false);

/*  Um den Zustand der Matrix als Spielfeld abzubilden,
 *  verwende ich ein Array mit 1024 Bytes. Jedes dieser
 *  Bytes besteht aus 8 Bits, die wie folgt verwendet werden:
 *
 *  Bit 0 - Spieler
 *  Bit 1 - Geist 1
 *  Bit 2 - Geist 2
 *  Bit 3 - Geist 3
 *  Bit 4 - Wand
 *  Bit 5 - Punkt
 *  Bit 6 - blinkender Punkt
 *  Bit 7 - Obst
 */

 /* Startspielfeld statisch erzeugen */
uint8_t spielfeld[32][32] = { 
  { 16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,16,16,16,16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16,16,16,16,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,16 },
  { 16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16 }
};
const uint8_t SPIELER = 1;  // gelb                   7,4,0
const uint8_t GEIST1 = 2;   // hellblau               1,1,3
const uint8_t GEIST2 = 4;   // grün                   0,4,0
const uint8_t GEIST3 = 8;   // rot                    4,0,0
                            // blau wenn essbar       0,0,4
const uint8_t WAND =  16;   // orange                 3,1,0
const uint8_t PUNKT = 32;   // grau                   1,1,1
const uint8_t BLINK = 64;   // grau blinkend heller   2,2,2
const uint8_t OBST = 128;   // magenta                3,0,2

// noch ist die Welt leer, also kann ich den Spieler in die Mitte stellen
uint8_t spieler_position[2] = {15,15};
uint8_t neue_position[2];
uint8_t geist1_position[2]  = { 5, 5};
uint8_t geist2_position[2]  = { 5,15};
uint8_t geist3_position[2]  = {15, 5};

/*  Die Umrandung ist nur dann sinnvoll, wenn man sie als
 *  Anzeige benutzt, um Punktestand und Leben anzuzeigen.
 *  Ansonsten ist sie Verschwendung von knappem Bildschirm.
 */
void spielfeld_umranden() {
  for ( uint8_t i = 0; i < 32; i++ ) {
    spielfeld[i][0]  = WAND;      // rechts
    spielfeld[i][31] = WAND;      // links
    spielfeld[0][i]  = WAND;      // oben
    spielfeld[31][i] = WAND;      // unten
  }
}

void labyrinth_erzeugen() {
  //*
  // ein paar Mauerstücke von Hand einfügen
  spielfeld[17][17] = WAND;
  spielfeld[17][18] = WAND;
  spielfeld[17][19] = WAND;
  // und mit einer Schleife
  for ( int x = 12; x < 25; x ++ ) {
    int y = 23;
    spielfeld[x][y] = WAND;
  }
  //*/
}

void spieler_erzeugen() {
  spielfeld[spieler_position[0]][spieler_position[1]] = SPIELER;
}

uint16_t zaehle_punkte () {
  uint16_t anzahl = 0;  // sicherheitshalber auf Null setzen
  for ( uint8_t x = 0; x < 32; x++ ){
    for ( uint8_t y = 0; y < 32; y++ ) {
      if ( spielfeld[x][y] == PUNKT ) {
        anzahl++;
      }
    }
  }
  return anzahl;
}

void setup() {
  // start serial port at 9600 bps
  Serial.begin(9600);
  // RGB-LED-Matrix aktivieren
  matrix.begin();
  // Ein Spielfeld erzeugen
  spielfeld_umranden();
  labyrinth_erzeugen();
  spieler_erzeugen();
  // einmal am Anfang, später muss das bei jedem neuen Level erfolgen!
  anzahl_punkte = zaehle_punkte();
  delay(1000);
  Serial.print("Setup komplett.\r\n");
}

/*  Hier findet die Prüfung statt, ob die Bewegung geht und ob etwas passiert.
 *  Alles, was passiert, passiert in Funktionen - die es noch nicht gibt.
 */
int pruefe_position() {
  int result;
  switch ( spielfeld[neue_position[0]][neue_position[1]] ) {
    case GEIST1:
    case GEIST2:
    case GEIST3:
      // leben_verloren();  // Was braucht es alles, damit PacMan ein Leben verlieren kann?
      result = 14;          // 2 + 4 + 8 = 14
      break;
    case WAND:
      result = WAND;    // die Wand ist undurchdringbar
      break;
    case PUNKT:
      Serial.print(anzahl_punkte);Serial.print(" ");
      if ( anzahl_punkte % 32 == 0 ) { Serial.println(""); }
      anzahl_punkte--;  // ein Punkt ist gefressen.
      result = PUNKT;
      break;
    case BLINK:
      result = BLINK;
      break;
    case OBST:
      result = OBST;
      break;
    default:
      result = 0;
      break;
  }
  return result;
}

/*  Die vier Funktionen hoch(), runter(), links(), rechts()
 *  berechnen nur die neuen Koordinaten. Prüfung und die tatsächliche
 *  Bewegung kommt in die neue Funktion spieler_bewegen()
 */
void spieler_hoch() {
  neue_position[0] = spieler_position[0] - 1;    // neue Postion berechnen
  neue_position[1] = spieler_position[1];
  spieler_bewegen();
}

void spieler_runter() {
  neue_position[0] = spieler_position[0] + 1;    // neue Postion berechnen
  neue_position[1] = spieler_position[1];
  spieler_bewegen();
}

void spieler_links() {
  neue_position[0] = spieler_position[0];
  neue_position[1] = spieler_position[1] + 1;    // neue Postion berechnen
  spieler_bewegen();
}

void spieler_rechts() {
  neue_position[0] = spieler_position[0];
  neue_position[1] = spieler_position[1] - 1;    // neue Postion berechnen
  spieler_bewegen();
}

/*  spieler_bewegen() enthält alle Prüfungen und setzt am Ende 
 *  nach allen Prüfungen den Spieler neu. 
 *  ToDo: Zukünftig braucht es das auch für die Geister.
 */
void spieler_bewegen() {
  int ergebnis = pruefe_position();
  if ( ergebnis != WAND ) {
    // Der reale Rand der Matrix ist ein Rand.
    if ( ( spieler_position[0] > 0 )  && ( spieler_position[1] < 31 ) && \
       ( spieler_position[0] < 31 ) && ( spieler_position[1] > 0 ) ) {
      spielfeld[spieler_position[0]][spieler_position[1]] = 0;  // alte Stelle löschen
      spieler_position[0] = neue_position[0];
      spieler_position[1] = neue_position[1];
      spielfeld[spieler_position[0]][spieler_position[1]] = SPIELER;
    }
  }

}

/*  Mit dieser Funktion steuert man den Pac-Man,
 *  der hier überall spieler heißt.
 */
void spieler_steuern() {
  // WASD von serial lesen
  if (Serial.available() > 0) {
    // get incoming byte:
    int richtung = Serial.read();
    switch ( richtung ) {
      case 'w':
      case 'W':
        spieler_hoch();
        break;
      case 'a':
      case 'A':
        spieler_links();
        break;
      case 's':
      case 'S':
        spieler_runter();
        break;
      case 'd':
      case 'D':
        spieler_rechts();
        break;
    }
  }
}

void matrix_zeichnen() {
  // Anhand der Werte in spielfeld[][] die Pixel setzen
  uint8_t r, g, b;  // Zwischenlager für die Farbwerte rot, grün, blau
  for ( uint8_t x = 0; x < 32; x++ ) {
    for ( uint8_t y = 0; y < 32; y++ ) {
      //matrix.drawPixel (x, y, matrix.Color444(0,0,0) );
      switch ( spielfeld[x][y] ) {
        case SPIELER:
          r = 7; g = 5; b = 0;
          break;
        case GEIST1:
          r = 1; g = 1; b = 3;
          break;
        case GEIST2:
          r = 0; g = 4; b = 0;
          break;
        case GEIST3:
          r = 4; g = 0; b = 0;
          break;
        case WAND:
          r = 3; g = 1; b = 0;
          break;
        case PUNKT:
          r = 1; g = 1; b = 1;
          break;
        case BLINK:
          r = 2; g = 2; b = 2;
          break;
        case OBST:
          r = 3; g = 0; b = 2;
          break;
        default:      // wenn _keiner_ der obigen Fälle zutrifft, dann schwarz
          r = 0; g = 0; b = 0;
          break;
      }
      matrix.drawPixel( x, y, matrix.Color444(r, g, b) );
    }
  }
}

void spielfeld_voller_punkte () {
  for ( uint8_t x = 0; x < 32; x++ ){
    for ( uint8_t y = 0; y < 32; y++ ) {
      spielfeld[x][y] = PUNKT;
    }
  }
}

/*  Das neue Level ist erst einmal kein neues Level.
 *  Stattdessen wird alles alte recylet und vorher einmal
 *  mit weißen Punkten übermalt. Jedoch bekommt man so nicht 
 *  das statisch definierte erste Level zurück :-(
 */
void neues_level () {
  spielfeld_voller_punkte();
  spielfeld_umranden();
  labyrinth_erzeugen();
  spieler_erzeugen();
  anzahl_punkte = zaehle_punkte();
}

void loop() {
  // put your main code here, to run repeatedly:
  spieler_steuern();
  // was sonst noch notwendig sein wird kann hier hin
  // wenn alle Punkte gefressen sind, neues Level starten
  if ( anzahl_punkte == 0 ) {
    Serial.println("Neues Level erreicht.");
    neues_level();
  }
  // einmal jede Runde das Bild neu zeichnen lassen
  matrix_zeichnen();
}
